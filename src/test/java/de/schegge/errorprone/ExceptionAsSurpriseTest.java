package de.schegge.errorprone;

import com.google.common.base.Predicates;
import com.google.errorprone.BugCheckerRefactoringTestHelper;
import com.google.errorprone.CompilationTestHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ExceptionAsSurpriseTest {
    private final CompilationTestHelper compilationHelper =
            CompilationTestHelper.newInstance(ExceptionAsSurprise.class, getClass());
    private final BugCheckerRefactoringTestHelper testHelper =
            BugCheckerRefactoringTestHelper.newInstance(ExceptionAsSurprise.class, getClass());

    @Test
    void noBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i != 42) {
                              throw new IllegalArgumentException();
                            } else {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            }
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void elseWithToManyLinesBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void singleLineBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            // BUG: Diagnostic matches: X
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .expectErrorMessage("X", Predicates.containsPattern("Throwing an Exception in the else statement is much harder to read"))
                .doTest();
    }

    @Test
    void suppressSingleLineBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          @SuppressWarnings("ExceptionAsSurprise")
                          public void test(int i) {
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void multipleLinesBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            // BUG: Diagnostic matches: X
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .expectErrorMessage("X", Predicates.containsPattern("Throwing an Exception in the else statement is much harder to read"))
                .doTest();
    }

    @Test
    void multipleLinesExceedingLimitBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .setArgs("-XepOpt:ExceptionAsSurprise:Lines=1")
                .doTest();
    }

    @Test
    void singleLineBugPatternRefactoring() {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i != 42) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void multipleLinesBugPatternRefactoring() {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i == 42) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (i != 42) {
                              System.out.println("Not the Answer to the Ultimate Question of Life, the Universe, and Everything");
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void booleanLiteralRefactoring() {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (true) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (false) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void unnecessaryParanthesisRefactoring() {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (((true))) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (false) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """)
                .doTest();
    }

    @Test
    void logicalComplementRefactoring() {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (!true) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """)
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (true) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """)
                .doTest();
    }

    @ParameterizedTest
    @CsvSource({
            "i == 42,i != 42",
            "i != 42,i == 42",
            "i < 42,i >= 42",
            "i >= 42,i < 42",
            "i > 42,i <= 42",
            "i <= 42,i > 42",
    })
    void relationalRefactoring(String expected, String input) {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (EXPRESSION) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """.replace("EXPRESSION", input))
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (EXPRESSION) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """.replace("EXPRESSION", expected))
                .doTest();
    }

    @ParameterizedTest
    @CsvSource({
            "i >= 42 && i <= 42,i < 42 || i > 42",
            "i < 42 || i > 42,i >= 42 && i <= 42",
            "i >= 42 & i <= 42,i < 42 | i > 42",
            "i < 42 | i > 42,i >= 42 & i <= 42",
            "i >= 42 && i <= 42,(i < 42) || (i > 42)",
    })
    void deMorganRefactoring(String expected, String input) {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (EXPRESSION) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """.replace("EXPRESSION", input))
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(int i) {
                            if (EXPRESSION) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """.replace("EXPRESSION", expected))
                .doTest();
    }


    @ParameterizedTest
    @CsvSource({
            "b1 && !b2 || !b1 && b2, (!b1 || b2) && (b1 || !b2)",
            "b1 && (!b2 || b2) && !b1, !b1 || b2 && !b2 || b1",
            "(b1 || !b2) && (!b1 || b2), (!b1 && b2) || (b1 && !b2)",
            "(b1 || !b2) && (!b1 || b2), !b1 && b2 || b1 && !b2",
    })
    void booleanExpressionRefactoring(String expected, String input) {
        testHelper.addInputLines("Test.java", """
                        public class Test {
                          public void test(boolean b1, boolean b2) {
                            if (EXPRESSION) {
                              System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                            } else {
                              throw new IllegalArgumentException();
                            }
                          }
                        }
                        """.replace("EXPRESSION", input))
                .addOutputLines("Test.java", """
                        public class Test {
                          public void test(boolean b1, boolean b2) {
                            if (EXPRESSION) {
                              throw new IllegalArgumentException();
                            }
                            System.out.println("Answer to the Ultimate Question of Life, the Universe, and Everything");
                          }
                        }
                        """.replace("EXPRESSION", expected))
                .doTest();
    }
}