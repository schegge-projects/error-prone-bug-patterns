package de.schegge.errorprone;

import com.google.common.base.Predicates;
import com.google.errorprone.BugCheckerRefactoringTestHelper;
import com.google.errorprone.CompilationTestHelper;
import org.junit.jupiter.api.Test;

class SpyOnInterfaceTest {
    private final CompilationTestHelper compilationHelper =
            CompilationTestHelper.newInstance(SpyOnInterface.class, getClass());
    private final BugCheckerRefactoringTestHelper testHelper =
            BugCheckerRefactoringTestHelper.newInstance(SpyOnInterface.class, getClass());

    @Test
    void noBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        import org.mockito.Mock;
                        import org.mockito.Spy;
                                                
                        public class Test {
                          interface TestService {
                          }
                          
                          @Mock
                          private TestService testService;
                          
                          @Spy
                          private TestService initializedTestService = new TestService() {};

                          @Spy
                          @SuppressWarnings("SpyOnInterface")
                          private TestService suppressedTestService;
                        }
                        """)
                .doTest();
    }

    @Test
    void spyOnInterfaceBugPattern() {
        compilationHelper.addSourceLines("Test.java", """
                        import org.mockito.Mock;
                        import org.mockito.Spy;
                                                
                        public class Test {
                          interface TestService {
                          }
                          
                          @Spy
                          // BUG: Diagnostic matches: X
                          private TestService testService;
                        }
                        """)
                .expectErrorMessage("X", Predicates.containsPattern("A @Spy on an interface is only a @Mock"))
                .doTest();
    }

    @Test
    void spyOnInterfaceBugPatternRefactoring() {
        testHelper.addInputLines("Test.java", """
                        import org.mockito.Mock;
                        import org.mockito.Spy;
                                                
                        public class Test {
                          interface TestService {
                          }
                          
                          @Spy
                          private TestService testService;
                        }
                        """)
                .addOutputLines("Test.java", """
                        import org.mockito.Mock;
                        import org.mockito.Spy;
                                                
                        public class Test {
                          interface TestService {
                          }
                          
                          @Mock
                          private TestService testService;
                        }
                        """)
                .doTest();
    }
}